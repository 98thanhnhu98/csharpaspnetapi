﻿using AppDemoAuthenJwt.Migrations;
using AppDemoAuthenJwt.Models;
using Microsoft.EntityFrameworkCore;

namespace AppDemoAuthenJwt.Service
{
    public interface IOrderServices
    {
        Task<IEnumerable<Order>> getAllOrder();
        Task SaveOrder(Order order);
        Task<Order?> GetOrdertById(long id);
        Task<bool> DeleteOrder(long id);
        Task<bool> UpdateOrder(long id, Order order);
    }
    public class OrderService : IOrderServices
    {
        private readonly IConfiguration _config;
        private readonly ILogger<Order> _logger;
        private readonly PracticeDbContext _context;

        public OrderService(IConfiguration config, ILogger<Order> logger,
         PracticeDbContext context)
        {
            _config = config;
            _logger = logger;
            _context = context;
        }

        public async Task<bool> DeleteOrder(long id)
        {
            var order = await _context.Orders.FindAsync(id);
            if (order == null)
            {
                return false;
            }
            _context.Orders.Remove(order);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<IEnumerable<Order>> getAllOrder()
        {
            try
            {
                var listDb = await _context.Orders.ToListAsync();
                if (listDb.Count() > 0)
                {
                    return listDb;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Data order is null , log : " + ex.Message);
            }
            return null;
        }

        public async Task<Order?> GetOrdertById(long id)
        {
            return await _context.Orders.FindAsync(id);
        }

        public async Task SaveOrder(Order order)
        {
            _context.Orders.AddAsync(order);
            await _context.SaveChangesAsync();
        }

        public async Task<bool> UpdateOrder(long id, Order order)
        {
            if (id != order.ProductId)
            {
                return false;
            }

            _context.Entry(order).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderExists(id))
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }

            return true;
        }

        private bool OrderExists(long id)
        {
            return _context.Orders.Any(e => e.ProductId == id);
        }
    }
}
