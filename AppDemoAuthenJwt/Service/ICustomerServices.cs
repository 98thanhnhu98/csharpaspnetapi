﻿using AppDemoAuthenJwt.Common;
using AppDemoAuthenJwt.Migrations;
using AppDemoAuthenJwt.Models;
using Microsoft.EntityFrameworkCore;

namespace AppDemoAuthenJwt.Service
{
    public interface ICustomerServices
    {
        Task<IEnumerable<Customer>> getAllCustomer(Filter? filter);
        Task SaveCustomer(Customer customer);
        Task<Customer?> GetCustomerById(long id);
        Task<bool> DeleteCustomer(long id);
        Task<bool> UpdateCustomer(long id, Customer customer);
    }
    public class CustomerService : ICustomerServices
    {
        private readonly IConfiguration _config;
        private readonly ILogger<Customer> _logger;
        private readonly PracticeDbContext _context;

        public CustomerService(IConfiguration config, ILogger<Customer> logger,
         PracticeDbContext context)
        {
            _config = config;
            _logger = logger;
            _context = context;
        }

        public async Task<bool> DeleteCustomer(long id)
        {
            var feedback = await _context.Customers.FindAsync(id);
            if (feedback == null)
            {
                return false;
            }
            _context.Customers.Remove(feedback);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<IEnumerable<Customer>> getAllCustomer(Filter? filter)
        {
            try
            {
                var query = _context.Customers.AsQueryable();
                if (filter.Filters > 0)
                {
                    query = query.Where(c => c.Status == filter.Filters);
                }
                if (!string.IsNullOrEmpty(filter.Search))
                {
                    query = query.Where(c => c.Name.Contains(filter.Search));
                }
                var listDb = await query.ToListAsync();
                if (listDb.Count() > 0)
                {
                    return listDb;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Data Feedback is null , log : " + ex.Message);
            }
            return null;
        }

        public async Task<Customer?> GetCustomerById(long id)
        {
            return await _context.Customers.FindAsync(id);
        }

        public async Task SaveCustomer(Customer customer)
        {
            _context.Customers.AddAsync(customer);
            await _context.SaveChangesAsync();
        }

        public async Task<bool> UpdateCustomer(long id, Customer customer)
        {
            if (id != customer.Id)
            {
                return false;
            }

            _context.Entry(customer).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CutomerExists(id))
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }

            return true;
        }

        private bool CutomerExists(long id)
        {
            return _context.Customers.Any(e => e.Id == id);
        }
    }
}
