﻿using AppDemoAuthenJwt.Common;
using AppDemoAuthenJwt.Migrations;
using AppDemoAuthenJwt.Models;
using AppDemoAuthenJwt.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Data;

namespace AppDemoAuthenJwt.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerServices _services;
        private readonly PracticeDbContext _context;

        public CustomerController(ICustomerServices services,PracticeDbContext practice)
        {
            _services = services;
            _context = practice;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ReponseGetOneDto>> GetOne(long id)
        {
            var rol = await _services.GetCustomerById(id);
            
            var orders = await _context.Orders.Where(o => o.CustomerId == id).ToListAsync();

            if(rol == null)
            {
                return Ok("Object is null");
            }
            var dto = new ReponseGetOneDto()
            {
                Name = rol.Name,
                Age = rol.Age,
                Gender = rol.Gender,
                Description = rol.Description,
                Status = rol.Status,
                Address = rol.Address,
                Orders = orders,
            };

            return Ok(new CommonResponse<ReponseGetOneDto>(dto));
        }


        [HttpGet]
        public async Task<ActionResult<CommonResponse<IEnumerable<Customer>>>> GetList([FromQuery] Filter? filter)
        {

            //var validFilter = new
            var rol = await _services.getAllCustomer(filter);
            
            return Ok(new CommonResponse<IEnumerable<Customer>>(rol));
        }

        [HttpPost]
        public async Task<ActionResult<Customer>> add(Customer customer)
        {
            await _services.SaveCustomer(customer);
            return CreatedAtAction(nameof(GetList), new { id = customer.Id }, customer);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRole(long id)
        {
            var success = await _services.DeleteCustomer(id);

            if (success)
            {
                return Ok();
            }
            else
            {
                return Ok("Object nofound");
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(long id, Customer customer)
        {
            var success = await _services.UpdateCustomer(id, customer);

            if (success)
            {
                return Ok();
            }
            else
            {
                return Ok("Object nofound");
            }
        }
    }
}
