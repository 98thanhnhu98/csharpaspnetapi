﻿using AppDemoAuthenJwt.Common;
using AppDemoAuthenJwt.Models;
using AppDemoAuthenJwt.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AppDemoAuthenJwt.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderServices _services;

        public OrderController(IOrderServices services)
        {
            _services = services;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Order>> GetOne(long id)
        {
            var rol = await _services.GetOrdertById(id);
            if (rol == null)
            {
                return NotFound();
            }
            return Ok(new CommonResponse<Order>(rol));
        }


        [HttpGet]
        public async Task<ActionResult<CommonResponse<IEnumerable<Order>>>> GetList()
        {
            //var validFilter = new
            var rol = await _services.getAllOrder();
            if (rol == null)
            {
                return NotFound();
            }
            return Ok(new CommonResponse<IEnumerable<Order>>(rol));
        }

        [HttpPost]
        public async Task<ActionResult<Order>> addRole(Order order)
        {
            await _services.SaveOrder(order);
            return CreatedAtAction(nameof(GetList), new { id = order.ProductId }, order);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRole(long id)
        {
            var success = await _services.DeleteOrder(id);

            if (success)
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateRole(long id, Order order)
        {
            var success = await _services.UpdateOrder(id, order);

            if (success)
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }
    }
}
