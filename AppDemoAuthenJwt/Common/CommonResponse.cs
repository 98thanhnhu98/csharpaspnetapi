﻿namespace AppDemoAuthenJwt.Common
{
    public class CommonResponse<T>
    {
        public CommonResponse() { }

        public CommonResponse(T data)
        {
            Successed = true;
            Message = string.Empty;
            Error = null;
            Data = data;
        }

        public T Data { get; set; }
        public bool Successed { get; set; }
        public string[] Error { get; set; }
        public string Message { get; set; }
    }
}
