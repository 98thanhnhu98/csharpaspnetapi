﻿using AppDemoAuthenJwt.Models;

namespace AppDemoAuthenJwt.Common
{
    public class ReponseGetOneDto
    {
        public string Name { get; set; } = null!;

        public int? Age { get; set; }

        public string? Gender { get; set; }

        public string? Address { get; set; }

        public int Status { get; set; }

        public string? Description { get; set; }


/// <summary>
/// 
/// </summary>
/// 
        public virtual ICollection<Order>? Orders { get; set; }

        public ReponseGetOneDto(string name , int age, string gender , string address , int status , string desc , ICollection<Order> order)
        {
            Name = name;
            Age = age;
            Gender = gender;
            Address = address;
            Status = status;
            Description = desc;
            Orders = order;
        }

        public ReponseGetOneDto()
        {
        }
    }
}
