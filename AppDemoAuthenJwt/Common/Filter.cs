﻿namespace AppDemoAuthenJwt.Common
{
    public class Filter
    {
        public string? Search { get; set; }
        public int? Filters { get; set; }

        public Filter(string search, int filter)
        {
            Search = search;
            Filters = filter;
        }
        public Filter()
        {

        }
    }
}
